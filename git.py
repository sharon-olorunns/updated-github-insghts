#based off of https://github.com/gomesfernanda/some-github-metrics and https://towardsdatascience.com/github-user-insights-using-github-api-data-collection-and-analysis-5b7dca1ab214
# encoding=utf8
from requests.auth import HTTPBasicAuth
import pandas as pd
import numpy as np
import requests
import json
import sys
import getpass
reload(sys)
sys.setdefaultencoding('utf8')


# Get the Github User Details up here
username = raw_input("What is your username? ")
password = getpass.getpass("What is your password? ")

# ------------------------------------------------------------------------------------------------------------
#credentials = json.loads(open('credentials.json').read())
authentication = HTTPBasicAuth(
    username, password)

data = requests.get('https://api.github.com/users/' +
                    username, auth=authentication)

if data.status_code == 200:
    print('You have successfully entered your login details. Welcome to Github Insights! \n')
else:
    print('Incorrect details. Please run the program again')
    exit()

data = data.json()


# ------------------------------------------------------------------------------------------------------------
# Return basic info about the User
print("Welcome " + username)
print("Name: {}".format(data['name']))
print("Email: {}".format(data['email']))
print("Location: {}".format(data['location']))
print("Public repos: {}".format(data['public_repos']))
print("Public gists: {}".format(data['public_gists']))
print("About: {}\n".format(data['bio']))


# ------------------------------------------------------------------------------------------------------------
# Collect all the necessary repository data for that Github User
print("Collecting repositories information")
url = data['repos_url']
page_no = 1
repos_data = []
while (True):
    response = requests.get(url, auth=authentication)
    response = response.json()
    repos_data = repos_data + response
    repos_fetched = len(response)
    if (repos_fetched == 30):
        page_no = page_no + 1
        url = data['repos_url'] + '?page=' + str(page_no)
    else:
        break

repos_information = []
for i, repo in enumerate(repos_data):
    data = []
    data.append(repo['id'])
    data.append(repo['name'])
    data.append(repo['description'])
    data.append(repo['created_at'])
    data.append(repo['updated_at'])
    data.append(repo['owner']['login'])
    data.append(repo['license']['name'] if repo['license'] != None else None)
    data.append(repo['has_wiki'])
    data.append(repo['forks_count'])
    data.append(repo['open_issues_count'])
    data.append(repo['stargazers_count'])
    data.append(repo['watchers_count'])
    data.append(repo['url'])
    data.append(repo['commits_url'].split("{")[0])
    data.append(repo['url'] + '/languages')
    repos_information.append(data)


# ------------------------------------------------------------------------------------------------------------
# Create a csv with all the information about a users public gists
repos_df = pd.DataFrame(repos_information, columns=['Id', 'Name', 'Description', 'Created on', 'Updated on',
                                                    'Owner', 'License', 'Includes wiki', 'Forks',
                                                    'Issues', 'Stars', 'Watchers',
                                                    'Repo URL', 'Commits URL', 'Languages URL'])


# ------------------------------------------------------------------------------------------------------------
# Get information about the languages used by the Github User
print("Collecting language data")
for i in range(repos_df.shape[0]):
    response = requests.get(
        repos_df.loc[i, 'Languages URL'], auth=authentication)
    response = response.json()
    if response != {}:
        languages = []
        for key, value in response.items():
            languages.append(key)
        languages = ', '.join(languages)
        repos_df.loc[i, 'Languages'] = languages
    else:
        repos_df.loc[i, 'Languages'] = ""
print("Language data collection complete")
repos_df.to_csv('./data/repos_info.csv', index=False)
print("Saved repositories information to repo_info.csv\n")


# ------------------------------------------------------------------------------------------------------------
# Get more information about single commits made by the Github User
print("Collecting commits information")
commits_information = []
for i in range(repos_df.shape[0]):
    url = repos_df.loc[i, 'Commits URL']
    page_no = 1
    while (True):
        response = requests.get(url, auth=authentication)
        response = response.json()
        for commit in response:
            commit_data = []
            commit_data.append(repos_df.loc[i, 'Id'])
            commit_data.append(commit['sha'])
            commit_data.append(commit['commit']['committer']['date'])
            commit_data.append(commit['commit']['message'])
            commits_information.append(commit_data)
        if (len(response) == 30):
            page_no = page_no + 1
            url = repos_df.loc[i, 'Commits URL'] + '?page=' + str(page_no)
        else:
            break


# ------------------------------------------------------------------------------------------------------------
# ----------------------------- REPOSITORY INFORMATION

commits_df = pd.DataFrame(commits_information, columns=[
                          'Repo Id', 'Commit Id', 'Date', 'Message'])
commits_df.to_csv('./data/commits_info.csv', index=False)
print("Saved commits information to commits_info.csv")


repos = pd.read_csv('./data/repos_info.csv')
commits = pd.read_csv('./data/commits_info.csv')


print("Total repos till date: {}".format(repos.shape[0]))
print("Total commits till date: {}".format(commits.shape[0]))


# ------------------------------------------------------------------------------------------------------------
# ----------------------------- COMMITS PER REPOSITORY

commits_count = pd.DataFrame(pd.merge(repos,
                                      commits,
                                      left_on='Id',
                                      right_on='Repo Id',
                                      how='left').groupby('Id').size().reset_index())
commits_count.columns = ['Id', 'Commits count']

repos = pd.merge(repos, commits_count, on='Id')
repos.to_csv('./data/rep_com.csv', index=False)


# -----------------------------------------------------------------------------------------
# ----------------------------- COMMITS PER YEAR

commits['Year'] = commits['Date'].apply(lambda x: x.split('-')[0])
yearly_stats = commits.groupby('Year').count()['Commit Id']

# print(yearly_stats)
yearly_stats.reset_index().to_csv('./data/yearly_commits.csv', index=False)


# ------------------------------------------------------------------------------------------------------------
# ----------------------------- FREQUENCY OF A CERTAIN LANGUAGE
list_of_languages = []
for languages in repos['Languages']:
    if type(languages) == str:
        for language in languages.split(','):
            list_of_languages.append(language.strip())

languages_count = pd.Series(list_of_languages).value_counts()
# print(languages_count)

languages_count.to_csv('./data/langcsv.csv')

langA = pd.read_csv("./data/langcsv.csv", header=None, index_col=None)
langA.columns = ['Language', 'Count']
langA.to_csv("./data/langcsv.csv", index=False)
