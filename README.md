# Updated-Github-Insghts

This is a project that shows certain metrics for a GitHub user. It uses python, HTML, CSS and the d3.js to read, organize and visualize the data

In order to succesfully run this project download the repositiory. Open a new termnial window.
The user will be asked to enter their github username and their password. There password entry will be hidden to ensure privacy.

If the credentials entered are correct the python script will begin to scrap data from the users github profile. It will get the necessary
information and organize them into .csv files. There will be a series of messages in the command line that indicate that the script is running.

The next step requires you to have python SimpleHTTPServer running on your machine.
Once the script is completed, run the command python -m SimpleHTTPServer 8080 in you terminal. A port should be opened and you will be able to view the
website at the address http://localhost:8080/

The website displays the most frequent langugae used by the user, their number of commits by year and the number of commits per project

NOTE: the data file is left empty as the .csv files will be added when the script is run